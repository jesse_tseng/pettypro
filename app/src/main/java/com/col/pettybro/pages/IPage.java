package com.col.pettybro.pages;

import android.view.View;

/**
 * Created by Jesse on 16/6/19.
 */
public interface IPage {
    public String getTitle();
    public View getView();
}
