package com.col.pettybro.pages.subFramePages;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.col.pettybro.R;
import com.col.pettybro.pages.IPage;

/**
 * Created by Jesse on 16/6/19.
 */
public class PageProfile extends Fragment implements IPage {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sub_page_explore, null);

        return view;
    }

    @Override
    public String getTitle() {
        return "個人檔案";
    }
}
