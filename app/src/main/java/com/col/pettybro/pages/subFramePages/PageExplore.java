package com.col.pettybro.pages.subFramePages;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.col.pettybro.R;
import com.col.pettybro.models.User;
import com.col.pettybro.pages.IPage;
import com.col.pettybro.utils.ImageUtils;
import com.col.pettybro.utils.LocationService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Jesse on 16/6/19.
 */
public class PageExplore extends Fragment implements IPage, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    GoogleMap googleMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sub_page_explore, null);


        MapView mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        googleMap = mapView.getMap();
        googleMap.setMyLocationEnabled(true);




        MapsInitializer.initialize(getActivity().getApplicationContext());

        for (int i=0 ; i<5 ; i++) {
            User user = new User(R.drawable.pic2);
            user.location = new LatLng(Math.random()*10000/100000+24.160, Math.random()*10000/1000000+120.692);
            addAnnotation(this.getActivity(), googleMap, user);
        }


        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            setCurrentLocation();
        }
    }

    @Override
    public String getTitle() {
        return "發現";
    }




    public void setCurrentLocation() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... arg0) {
                //while (LocationService.getLocation()==null);
                return null;
            }
            @Override
            protected void onPostExecute(Void result) {

                Location location = LocationService.getLocation();


                LatLng position =  new LatLng(24.160561, 120.692210);

                googleMap.setOnMarkerClickListener(PageExplore.this);
                googleMap.setOnInfoWindowClickListener(PageExplore.this);


                CameraPosition cameraPosition = new CameraPosition.Builder().target(position).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }

        }.execute();
    }

    public static void addAnnotation(Context context, GoogleMap googleMap, User user) {

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), user.photoRes);
        bitmap = ((BitmapDrawable)ImageUtils.getRoundedShape(bitmap)).getBitmap();

        MarkerOptions marker = new MarkerOptions().position(user.location).title("毛小孩中途之家").snippet("台中市科園路21號");
        bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true);

        marker.icon(BitmapDescriptorFactory.fromBitmap(bitmap));

        googleMap.addMarker(marker);

    }


    @Override
    public boolean onMarkerClick(Marker marker) {Log.e("jesse log", "sadfa");
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Log.e("jesse log", "123");
    }
}
