package com.col.pettybro.pages.subFramePages;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.col.pettybro.R;
import com.col.pettybro.models.RecyclerViewAdapter;
import com.col.pettybro.models.User;
import com.col.pettybro.pages.IPage;
import java.util.ArrayList;


/**
 * Created by Jesse on 16/6/19.
 */
public class PageNews extends Fragment implements IPage {

    View view;
    private ArrayList<User> users;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.sub_page_news, null);


        setAdapter((RecyclerView) view.findViewById(R.id.list1));
        setAdapter((RecyclerView) view.findViewById(R.id.list2));
        setAdapter((RecyclerView) view.findViewById(R.id.list3));

        return view;
    }

    @Override
    public String getTitle() {
        return "焦點";
    }


    private void setAdapter(RecyclerView list) {

        users = new ArrayList<User>();
        for (int i=0 ; i<30 ; i++) users.add(new User(imgs[(int)(Math.random()*5)]));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        list.setLayoutManager(layoutManager);
        list.addItemDecoration(new RecyclerViewAdapter.SpacesItemDecoration(30));
        list.setAdapter(new RecyclerViewAdapter(users));
    }

    int[] imgs = {R.drawable.pic1, R.drawable.pic2, R.drawable.pic3, R.drawable.pic4, R.drawable.pic5};





}
