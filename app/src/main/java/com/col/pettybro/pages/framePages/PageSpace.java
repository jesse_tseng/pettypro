package com.col.pettybro.pages.framePages;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.NestedScrollingChild;
import android.support.v4.view.NestedScrollingChildHelper;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.col.pettybro.R;
import com.col.pettybro.models.RecyclerViewAdapter;
import com.col.pettybro.models.User;
import com.col.pettybro.pages.subFramePages.PageNews;
import com.col.pettybro.ui.component.ListView;
import com.col.pettybro.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

import static android.support.design.widget.TabLayout.Tab;

/**
 * Created by Jesse on 16/6/23.
 */
public class PageSpace extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private String[] pageTitles = {"動態", "個人資料", "相簿"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_space);

        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        appBarLayout.addOnOffsetChangedListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        TabLayout tabs = (TabLayout) findViewById(R.id.spaceTabs);


        Bundle extra = this.getIntent().getExtras();
        User user = extra.getParcelable("user");

        ((ImageView)findViewById(R.id.spaceCover)).setImageResource(user.photoRes);


        Drawable drawable = this.getResources().getDrawable(user.photoRes);
        ((ImageView)findViewById(R.id.spaceImg)).setImageDrawable(ImageUtils.getRoundedShape(drawable));


        ViewPager viewpager = (ViewPager) findViewById(R.id.viewpager);
        tabs.setupWithViewPager(viewpager);

        viewpager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view==object;
            }

            @Override
            public Object instantiateItem (ViewGroup container, int position)
            {
                View view;
                if (position==0) {
                    view = new ListView(PageSpace.this);
                    ArrayList<String> arr = new ArrayList<String>();
                    for (int i = 0; i < 30; i++) arr.add("");

                    ((ListView)view).setAdapter(new ArrayAdapter<String>(PageSpace.this, R.layout.listitem_wall, R.id.wallText, arr));
                }
                else {
                    view = LayoutInflater.from(PageSpace.this).inflate(R.layout.sub_page_profile, null);
                }

                container.addView(view);
                return view;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View)object);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return pageTitles[position];
            }
        });


    }


    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;

        ImageView img = (ImageView) appBarLayout.findViewById(R.id.spaceImg);
        if (percentage>.2 && img.getVisibility()==View.VISIBLE) {
            Animation an = new AlphaAnimation(1, 0);
            an.setDuration(1000);
            img.startAnimation(an);
            img.setVisibility(View.INVISIBLE);
        }
        else if (percentage<=.2 && img.getVisibility()==View.INVISIBLE) {
            Animation an = new AlphaAnimation(0, 1);
            an.setDuration(1000);
            img.startAnimation(an);
            img.setVisibility(View.VISIBLE);
        }


        //appBarLayout.findViewById(R.id.spaceImg).setAlpha(1-percentage);

        Log.e("jesse log", verticalOffset+", "+percentage);
    }
}
