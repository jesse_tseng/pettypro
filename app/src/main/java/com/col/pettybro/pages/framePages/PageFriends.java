package com.col.pettybro.pages.framePages;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

import com.col.pettybro.R;
import com.col.pettybro.pages.IPage;
import com.col.pettybro.pages.subFramePages.PageExplore;
import com.col.pettybro.pages.subFramePages.PageNews;
import com.col.pettybro.pages.subFramePages.PageProfile;

/**
 * Created by Jesse on 16/6/19.
 */
public class PageFriends extends Fragment {

    IPage[] pages = {new PageNews(), new PageExplore(), new PageProfile()};

    public PageFriends() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.page_friends, null);
        ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
        pager.setAdapter(new FragmentPagerAdapter(this.getFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return (Fragment) pages[position];
            }

            @Override
            public int getCount() {
                return pages.length;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return pages[position].getTitle();
            }
        });
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                FloatingActionButton fab = (FloatingActionButton) pages[position].getView().findViewById(R.id.fab);
                if (fab==null) return;
                Animation an = new ScaleAnimation(0f, 1f, 0f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                an.setDuration(200);
                fab.startAnimation(an);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout tabs = (TabLayout) getActivity().findViewById(R.id.header_tabs);
        tabs.setupWithViewPager(pager);

        return view;
    }
}
