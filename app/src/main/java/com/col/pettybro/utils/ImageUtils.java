package com.col.pettybro.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/**
 * Created by Jesse on 16/7/10.
 */
public class ImageUtils {


    public static Drawable getRoundedShape(Drawable drawable) {
        Bitmap scaleBitmapImage = ((BitmapDrawable)drawable).getBitmap();
        return getRoundedShape(scaleBitmapImage);
    }
    public static Drawable getRoundedShape(Bitmap scaleBitmapImage) {
        if (scaleBitmapImage==null) return null;
        int targetWidth = 300;
        int targetHeight = 300;
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth), ((float) targetHeight)) / 2), Path.Direction.CCW);

        canvas.clipPath(path);
        Bitmap sourceBitmap = scaleBitmapImage;
        canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight()), new Rect(0, 0, targetWidth, targetHeight), null);
        return new BitmapDrawable(targetBitmap);
    }

}
