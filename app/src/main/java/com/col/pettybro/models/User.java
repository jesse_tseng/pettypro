package com.col.pettybro.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Jesse on 16/6/26.
 */
public class User implements Parcelable {

    String name;
    String photoUrl;
    String coverUrl;

    public LatLng location;

    public int photoRes;

    public User(int photoRes) {
        this.photoRes = photoRes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(photoRes);
    }

    public static final Parcelable.Creator<User> CREATOR = new Creator(){

        @Override
        public User createFromParcel(Parcel source) {
            User user = new User(source.readInt());
                return user;
            }
        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
