package com.col.pettybro.models;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.col.pettybro.R;
import com.col.pettybro.pages.framePages.PageSpace;

import java.util.ArrayList;

/**
 * Created by Jesse on 16/7/1.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter {

    ArrayList arr;

    public RecyclerViewAdapter(ArrayList arr) {
        this.arr = arr;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_pic_text, parent, false);

        RecyclerView.ViewHolder viewHolder = new RecyclerView.ViewHolder(v) {};

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((ImageView)holder.itemView.findViewById(R.id.img)).setImageResource(((User)arr.get(position)).photoRes);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), PageSpace.class);
                i.putExtra("user", (Parcelable) arr.get(position));
                view.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arr.size();
    }



    public static class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.top = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.left = parent.getChildLayoutPosition(view) == 0 ? 0 : space;

        }
    }

}